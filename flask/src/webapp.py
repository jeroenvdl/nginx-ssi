from flask import Flask
import logging
import sys

app = Flask(__name__)

# Configure Logging
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.DEBUG)

@app.route("/webapp.py")
def main():
    return '''<h2>Hello world!</h2>'''

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=8000)